# python-flask-example



## Install pyenv:
    - Reference source: https://github.com/pyenv/pyenv#installation
    - Install pyenv with homebrew:
        brew update
        brew install pyenv
    - Set up your shell environment for Pyenv:
        echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
        echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
        echo 'eval "$(pyenv init -)"' >> ~/.bashrc
## Install Python 3.8.15:
    - Run command: 
        pyenv install 3.8.15
    - check version python:
        python --version

## Install pipenv:
    - run command:
        python3 -m pip install pipenv

## Define version project
    example: using ver 3.6:
        pipenv --python 3.6 
